from random import shuffle
import random
import math

class House:
    def __init__(self, owner):
        self.owner = owner

    def __str__(self):
        return self.owner

class Map:
    def __init__(self):
        self.houses = []
        self.distances = []

    def addHouse(self, house):
        self.houses.append(house)

    def addDistance(self, house1, house2, distance):
        self.distances.append([house1, house2, distance])

    def getDistance(self, house1, house2):
        for i in range (len(self.distances)):
            frm = self.distances[i][0]
            to = self.distances[i][1]
            if(str(frm) == house1 and str(to) == house2 or
                str(frm) == house2 and str(to) == house1):
                return self.distances[i][2]
        print("Ga nemu rute antara " + house1 + " dan " + house2)

class Agent:
    def __init__(self, maps):
        self.populations = []
        self.maps = maps
        self.solution = []

    def initPopulation(self, destinations, n):
        result = []
        for i in range(n):
            route = []
            copy = destinations[:]
            shuffle(copy)
            route.append(copy)
            route.append(self.fitness(copy))
            result.append(route)
            if i > 0:
                self.solution = self.checkSolution(route, self.solution)
            elif i == 0:
                self.solution = route
        self.populations += result
        print("So far yang mendekati solusi adalah " + str(self.solution))
        return result

    def checkSolution(self, route1, route2):
        if(route1[1] < route2[1]):
            return route1
        return route2

    def tournament(self, populations):
        #choose 2 routes
        versus = []
        versus = random.sample(populations, 2)
        print(versus)

        #battle and return the fittest one
        if(versus[0][1] < versus[1][1]):
            return versus[0]
        else:
            return versus[1]

    def fitness(self, route):
        #the smaller the better
        fitness = 0
        for i in range(len(route)-1):
            fitness += self.maps.getDistance(route[i], route[i+1])
        return fitness

    def crossOver(self, route1, route2):
        #return 2 routes WITHOUT fitness detail
        splittedRoutes = self.splitRoutes(route1, route2)
        return self.appendRoutes(splittedRoutes)

    def splitRoutes(self, route1, route2):
        #split each routes into 2, resulting in 4
        result = []
        point = math.ceil(len(route1[0])/2)
        route1Front = route1[0][:point]
        route1End = route1[0][point:]
        route2Front = route2[0][:point]
        route2End = route2[0][point:]
        result.append(route1Front)
        result.append(route1End)
        result.append(route2Front)
        result.append(route2End)
        return result

    def appendRoutes(self, splittedRoutes):
        #append 4 sub-routes into 2 complete routes
        result = []
        route1 = splittedRoutes[0] + splittedRoutes[3]
        route2 = splittedRoutes[2] + splittedRoutes[1]
        result.append(route1)
        result.append(route2)
        return result

    def mutate(self, route):
        swap = route[2], route[4]
        route[4], route[2] = swap
        return route

    def addPopulation(self, route):
        if not self.isValid(route):
            return
        tuple = []
        tuple.append(route)
        tuple.append(self.fitness(route))
        self.populations.append(tuple)
        self.solution = self.checkSolution(tuple, self.solution) #update solution

    def isValid(self, route):
        if(len(route) != len(set(route))): #duplicate house exists
            return False
        return True
