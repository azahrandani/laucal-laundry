"""laucal_laundry URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views
import main.urls as main
from main.views import index as index_main
from main.views import WelcomeView
from main.views import PleaseWait
from main.views import Congratulation
from main.views import RouteView
from main.views import InputBodongView
from main.views import StartJourney
from main.views import routing
from main.views import signup
from main.views import login
from main.views import logout

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', index_main, name='index'),
    url(r'^welcome/', WelcomeView.as_view(), name='welcome'),
    url(r'^please_wait/', PleaseWait.as_view(), name='please_wait'),
    url(r'^congrats/', Congratulation.as_view(), name='congrats'),
    url(r'^route/', RouteView.as_view(), name='route'),
    url(r'^inputbodong/', InputBodongView.as_view(), name='inputbodong'),
    url(r'^routing', routing, name='routing'),
    url(r'^post', signup, name='post'),
    url(r'^signin', login, name='signin'),
    url(r'^logout', logout, name='logout'),
    url(r'^start_journey/', StartJourney.as_view(), name='start_journey')
]
