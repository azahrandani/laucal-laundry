from django.db import models

# Create your models here.
class User(models.Model):
    email = models.EmailField(max_length=200, default='xx@gmail.com', unique=True, primary_key=True)
    password = models.CharField(max_length=15, default='password')
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.email + " || " + self.name
