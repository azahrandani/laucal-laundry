from .backend import *

#Cabang laundry
house12 = House("Cabang Beji")
house13 = House("Cabang Kutek")
house14 = House("Cabang Kukel")
house15 = House("Cabang Serengseng")
house16 = House("Cabang Cilodong")

#Rumah pelanggan
house1 = House("Bu Joko")
house2 = House("Bu Ahmad")
house3 = House("Bu Beri")
house4 = House("Bu Dinda")
house5 = House("Bu Erlinda")
house6 = House("Bu Fanita")
house7 = House("Bu Glori")
house8 = House("Bu Heri")
house9 = House("Bu Indri")
house10 = House("Bu Jessica")
house11 = House("Bu Karina")

maps = Map()

maps.addHouse(house1)
maps.addHouse(house2)
maps.addHouse(house3)
maps.addHouse(house4)
maps.addHouse(house5)
maps.addHouse(house6)
maps.addHouse(house7)
maps.addHouse(house8)
maps.addHouse(house9)
maps.addHouse(house10)
maps.addHouse(house11)
maps.addHouse(house12)
maps.addHouse(house13)
maps.addHouse(house14)
maps.addHouse(house15)
maps.addHouse(house16)


allHouses = maps.houses
print("Print semua rumah dulu yuk!")
for i in range (len(allHouses)):
    print(allHouses[i])

maps.addDistance(house1, house2, 5)
maps.addDistance(house1, house3, 8)
maps.addDistance(house1, house4, 14)
maps.addDistance(house1, house5, 9)
maps.addDistance(house1, house6, 14)
maps.addDistance(house1, house7, 6)
maps.addDistance(house1, house8, 10)
maps.addDistance(house1, house9, 13)
maps.addDistance(house1, house10, 13)
maps.addDistance(house1, house11, 7)
maps.addDistance(house1, house12, 17)
maps.addDistance(house1, house13, 2)
maps.addDistance(house1, house14, 5)
maps.addDistance(house1, house15, 5)
maps.addDistance(house1, house16, 9)

maps.addDistance(house2, house3, 7)
maps.addDistance(house2, house4, 10)
maps.addDistance(house2, house5, 11)
maps.addDistance(house2, house6, 6)
maps.addDistance(house2, house7, 5)
maps.addDistance(house2, house8, 9)
maps.addDistance(house2, house9, 11)
maps.addDistance(house2, house10, 3)
maps.addDistance(house2, house11, 8)
maps.addDistance(house2, house12, 10)
maps.addDistance(house2, house13, 14)
maps.addDistance(house2, house14, 7)
maps.addDistance(house2, house15, 9)
maps.addDistance(house2, house16, 10)

maps.addDistance(house3, house4, 13)
maps.addDistance(house3, house5, 3)
maps.addDistance(house3, house6, 9)
maps.addDistance(house3, house7, 12)
maps.addDistance(house3, house8, 8)
maps.addDistance(house3, house9, 10)
maps.addDistance(house3, house10, 12)
maps.addDistance(house3, house11, 4)
maps.addDistance(house3, house12, 8)
maps.addDistance(house3, house13, 10)
maps.addDistance(house3, house14, 7)
maps.addDistance(house3, house15, 5)
maps.addDistance(house3, house16, 11)

maps.addDistance(house4, house5, 6)
maps.addDistance(house4, house6, 9)
maps.addDistance(house4, house7, 11)
maps.addDistance(house4, house8, 7)
maps.addDistance(house4, house9, 10)
maps.addDistance(house4, house10, 12)
maps.addDistance(house4, house11, 8)
maps.addDistance(house4, house12, 6)
maps.addDistance(house4, house13, 9)
maps.addDistance(house4, house14, 10)
maps.addDistance(house4, house15, 6)
maps.addDistance(house4, house16, 2)

maps.addDistance(house5, house6, 9)
maps.addDistance(house5, house7, 10)
maps.addDistance(house5, house8, 4)
maps.addDistance(house5, house9, 12)
maps.addDistance(house5, house10, 7)
maps.addDistance(house5, house11, 5)
maps.addDistance(house5, house12, 8)
maps.addDistance(house5, house13, 9)
maps.addDistance(house5, house14, 2)
maps.addDistance(house5, house15, 3)
maps.addDistance(house5, house16, 6)

maps.addDistance(house6, house7, 4)
maps.addDistance(house6, house8, 5)
maps.addDistance(house6, house9, 9)
maps.addDistance(house6, house10, 3)
maps.addDistance(house6, house11, 2)
maps.addDistance(house6, house12, 9)
maps.addDistance(house6, house13, 8)
maps.addDistance(house6, house14, 5)
maps.addDistance(house6, house15, 3)
maps.addDistance(house6, house16, 3)

maps.addDistance(house7, house8, 7)
maps.addDistance(house7, house9, 5)
maps.addDistance(house7, house10, 5)
maps.addDistance(house7, house11, 8)
maps.addDistance(house7, house12, 9)
maps.addDistance(house7, house13, 10)
maps.addDistance(house7, house14, 6)
maps.addDistance(house7, house15, 9)
maps.addDistance(house7, house16, 3)

maps.addDistance(house8, house9, 5)
maps.addDistance(house8, house10, 4)
maps.addDistance(house8, house11, 8)
maps.addDistance(house8, house12, 9)
maps.addDistance(house8, house13, 7)
maps.addDistance(house8, house14, 10)
maps.addDistance(house8, house15, 11)
maps.addDistance(house8, house16, 8)

maps.addDistance(house9, house10, 7)
maps.addDistance(house9, house11, 8)
maps.addDistance(house9, house12, 10)
maps.addDistance(house9, house13, 9)
maps.addDistance(house9, house14, 4)
maps.addDistance(house9, house15, 1)
maps.addDistance(house9, house16, 6)

maps.addDistance(house10, house11, 9)
maps.addDistance(house10, house12, 10)
maps.addDistance(house10, house13, 12)
maps.addDistance(house10, house14, 8)
maps.addDistance(house10, house15, 9)
maps.addDistance(house10, house16, 11)

maps.addDistance(house11, house12, 6)
maps.addDistance(house11, house13, 8)
maps.addDistance(house11, house14, 3)
maps.addDistance(house11, house15, 10)
maps.addDistance(house11, house16, 7)

maps.addDistance(house12, house13, 1)
maps.addDistance(house12, house14, 7)
maps.addDistance(house12, house15, 5)
maps.addDistance(house12, house16, 3)

maps.addDistance(house13, house14, 11)
maps.addDistance(house13, house15, 10)
maps.addDistance(house13, house16, 7)

maps.addDistance(house14, house15, 7)
maps.addDistance(house14, house16, 9)

maps.addDistance(house15, house16, 5)

allDistances = maps.distances
for i in range (len(allDistances)):
    house1 = allDistances[i][0]
    house2 = allDistances[i][1]
    print("Jarak dari " + str(house1) + " ke " + str(house2) + " adalah " + str(allDistances[i][2]) + " km.")

def generateRoute(frm, destinations):
    agent = Agent(maps)
    dest = destinations
    print("-------------------------------------")
    print("Let's make some init populations!")
    routes = agent.initPopulation(frm, dest, 4) #[[route], fitness]
    for i in range (len(routes)):
        print(routes[i])

    for i in range(10):
        print("-------------------------------------")

        print("Time for a tournament!")
        winner1 = agent.tournament(routes)
        print("Winner 1 is " + str(winner1))
        winner2 = agent.tournament(routes)
        print("Winner 2 is " + str(winner2))
        print("-------------------------------------")

        print("Let's cross it over!")
        integ = agent.crossOver(winner1, winner2, frm)
        print(integ)
        print("-------------------------------------")

        print("Let's try to mutate both routes!")
        mutated1 = agent.mutate(integ[0])
        print(mutated1)
        mutated2 = agent.mutate(integ[1])
        print(mutated2)
        print("-------------------------------------")

        print("Add them up to our initial populations and see what we get!")
        agent.addPopulation(mutated1, frm)
        agent.addPopulation(mutated2, frm)
        for i in range(len(agent.populations)):
            print(agent.populations[i])
        routes = agent.populations

    print("Final solution is " + str(agent.solution))
    return agent.solution

# dest = ["Bu Joko", "Bu Ahmad", "Bu Erlinda", "Bu Glori"]
# frm = "Cabang Kutek"
# generateRoute(frm, dest)
