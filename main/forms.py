from django import forms

class SignUp_Form(forms.Form):
    name = forms.CharField(label='name', max_length=100, widget=forms.TextInput(attrs=attrs))
    username = forms.CharField(label='username', max_length=100, widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(label='password',max_length=32, widget=forms.PasswordInput(attrs=attrs))
