from django.shortcuts import render
from django.views.generic import View
from .utils.execute import *
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response, redirect, reverse
from django.http import HttpResponse, HttpResponseRedirect
from .models import User

# Create your views here.
response = {}

def index(request):
    response = {'kelompok':'Monopoly'}
    return render(request, 'index.html', response)

@csrf_exempt
def routing(request):
    if request.method == "POST":
        hasil = {}
        route = {}
        jarak = 0

        dari = request.POST["dari"]
        tujuan = request.POST["tujuan"]

        dari = str(dari)
        tujuan = str(tujuan)
        daftarTujuan = tujuan.split(",")
        for i in range(len(daftarTujuan)):
            daftarTujuan[i] = daftarTujuan[i].lstrip()

        hasil = generateRoute(dari, daftarTujuan)
        route = hasil[0]
        jarak = hasil[1]

        response["route"] = route
        response["jarak"] = jarak

        request.session['route'] = route
        request.session['jarak'] = jarak

        print("apakah sampai sini?")
        print("selesai sudah")
        # return redirect('../route')

        return render(request, "please_wait.html", {'route': request.session['route'],'jarak': request.session['jarak']})

        # return response
        # return HttpResponse(response)

# def finalRoute(request):
#     return render(request, "route.html", routing(request))

@csrf_exempt
def login(request):
    if request.method == "POST":
        email = request.POST["email"]
        password = request.POST["password"]
        user = User.objects.all().filter(email=email, password=password).first()
        if user is None:
            return render(request, 'index.html', response)
        else:
            request.session['nama'] = user.name
            response['name'] = request.session['nama']
            return redirect('/welcome')

@csrf_exempt
def signup(request):
    if request.method == "POST":
        nama = request.POST["name"]
        email = request.POST["email"]
        password = request.POST["password"]
        assigned_user = User(email=email, name=nama, password=password)
        assigned_user.save()
        request.session['nama'] = assigned_user.name
        response['name'] = request.session['nama']
    return render(request, "welcome.html", {'name': request.session['nama']})

def logout(request):
    print("#==> auth logout")
    request.session.flush()  # menghapus semua session

    # messages.info(request, "Anda berhasil logout. Semua session Anda sudah dihapus")
    return HttpResponseRedirect(reverse('index'))

class WelcomeView(View):
    def get(self, request):
        return render(request, "welcome.html", {'name': request.session['nama']})

class RouteView(View):
    def get(self, request):
        return render(request, "route.html", {'route': request.session['route'],'jarak': request.session['jarak'], 'name': request.session['nama']})

class PleaseWait(View):
    def get(self, request):
        return render(request, "please_wait.html")

class Congratulation(View):
    def get(self, request):
        return render(request, "congrats.html", {'jarak': request.session['jarak']})

class InputBodongView(View):
    def get(self, request):
        return render(request, "inputbodong.html", {'name': request.session['nama']})

    def post(self, request, *args, **kwargs):
        return render(request, "route.html", response)

class StartJourney(View):
    def get(self, request):
        return render(request, "start_journey.html")

    # def post(self, request, *args, **kwargs):
    #     print("ke post StartJourney ga sih")
    #     routing(request)
    #     return render(request, "route.html", response)
