from django.conf.urls import url
from .views import index, logout
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^logout/$', logout, name='logout'),
]
