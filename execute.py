from backend import *

house1 = House("Bubu")
house2 = House("Baba")
house3 = House("Bebe")
house4 = House("Bibi")
house5 = House("Bobo")
maps = Map()

maps.addHouse(house1)
maps.addHouse(house2)
maps.addHouse(house3)
maps.addHouse(house4)
maps.addHouse(house5)

allHouses = maps.houses
print("Print semua rumah dulu yuk!")
for i in range (len(allHouses)):
    print(allHouses[i])

maps.addDistance(house1, house2, 5)
maps.addDistance(house2, house3, 7)
maps.addDistance(house1, house3, 8)
maps.addDistance(house3, house5, 3)
maps.addDistance(house3, house4, 23)
maps.addDistance(house2, house4, 10)
maps.addDistance(house2, house5, 11)
maps.addDistance(house1, house5, 9)
maps.addDistance(house1, house4, 14)
maps.addDistance(house5, house4, 20)

allDistances = maps.distances
for i in range (len(allDistances)):
    house1 = allDistances[i][0]
    house2 = allDistances[i][1]
    print("Jarak dari " + str(house1) + " ke " + str(house2) + " adalah " + str(allDistances[i][2]) + " km.")

agent = Agent(maps)
dest = ["Bubu", "Baba", "Bebe", "Bibi", "Bobo"]
print("-------------------------------------")
print("Let's make some init populations!")
routes = agent.initPopulation(dest, 4) #[[route], fitness]
for i in range (len(routes)):
    print(routes[i])

for i in range(10):
    print("-------------------------------------")
    print("Time for a tournament!")
    winner1 = agent.tournament(routes)
    print("Winner 1 is " + str(winner1))
    winner2 = agent.tournament(routes)
    print("Winner 2 is " + str(winner2))
    print("-------------------------------------")
    print("Let's cross it over!")
    integ = agent.crossOver(winner1, winner2)
    print(integ)
    print("-------------------------------------")
    print("Let's try to mutate both routes!")
    mutated1 = agent.mutate(integ[0])
    print(mutated1)
    mutated2 = agent.mutate(integ[1])
    print(mutated2)
    print("-------------------------------------")
    print("Add them up to our initial populations and see what we get!")
    agent.addPopulation(mutated1)
    agent.addPopulation(mutated2)
    for i in range(len(agent.populations)):
        print(agent.populations[i])
    routes = agent.populations
print("Solusi final adalah " + str(agent.solution))
